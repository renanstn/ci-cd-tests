# ci-cd-tests

Estudos sobre a pipeline de CI/CD do gitlab.

## Buildando o app

- Acesse a pasta `src/`

```sh
docker build -t test ./src/
```

## Rodando o app

```sh
docker run --rm test
```
